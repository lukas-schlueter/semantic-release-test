# Semantic-Release-Test

Testing https://github.com/semantic-release/gitlab

Commit messages should be formatted according to this: https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines

See https://github.com/semantic-release/commit-analyzer for more information.